<?php

namespace MTS\Testing\Traits\Entity;

trait Doctrine
{
    /**
     * Provides convenience method for testing collections. Supports a getter, setter, add and remove method.
     * This method assumes that a setter is always chainable and add and remove methods return true or false as per
     * the add and removeElement methods on Doctrine's ArrayCollection object.
     *
     * @param object $entity The entity to test a collection for
     * @param string $getter The method to test as the collection's getter
     * @param \ArrayCollection $default The object that should be identical to the collections default
     * @param string $setter The method to test as the collection's setter
     * @param \ArrayCollection $collection The object that should be set
     * @param string $addMethod The method to test as the collection's add method
     * @param mixed $addValue Can be an array of objects or a single object as long as they match the method's signature
     * @param string $removeMethod The method to test as the collection's remove method
     * @param mixed $removeValue Can be an array of objects or a single object as long as they match the method's signature
     */
    public function assertEntityCollection(
        $entity,
        $getter,
        $default,
        $setter = null,
        $collection = null,
        $addMethod = null,
        $addValue = null,
        $removeMethod = null,
        $removeValue = null
    ) {
        $args = func_get_args();

        //Assert getter exists
        //Assert getter is callable
        $this->assertTrue(
            method_exists($entity, $getter),
            'The specified getter method "' . $getter . '" does not exist.'
        );
        $this->assertTrue(
            is_callable(array($entity, $getter)),
            'The specified getter method "' . $getter . '" is not callable.'
        );

        //Assert default is returned from getter
        $this->assertSame($default, $entity->$getter());

        //Assert setter exists and is callable
        if (isset($args[3])) {
            $this->assertTrue(
                method_exists($entity, $setter),
                'The specified setter method "' . $setter . '" does not exist.'
            );
            $this->assertTrue(
                is_callable(array($entity, $setter)),
                'The specified setter method "' . $setter . '" is not callable.'
            );

            if (isset($args[4])) {
                //Assert that setter returns instance of entity
                $this->assertSame($entity, $entity->$setter($args[4]));
                //Assert the same array collection is returned that is set
                $this->assertSame($collection, $entity->$getter());
            }
        }


        //Assert add method exists and is callable
        if (isset($args[5])) {
            $this->assertTrue(
                method_exists($entity, $addMethod),
                'The specified add method "' . $addMethod . '" does not exist.'
            );
            $this->assertTrue(
                is_callable(array($entity, $addMethod)),
                'The specified add method "' . $addMethod . '" is not callable.'
            );

            if (isset($args[6])) {
                $collectionCount = count($entity->getMocks());
                if (is_array($args[6])) {
                    $addCount = count($args[6]);
                    $total = $collectionCount + $addCount;

                    foreach ($args[6] as $item) {
                        $this->assertInternalType('boolean', $entity->$addMethod($item));
                    }

                    $this->assertEquals($total, count($entity->getMocks()));
                } else {
                    $total = $collectionCount + 1;
                    $this->assertTrue($entity->$addMethod($args[6]));
                    $this->assertEquals($total, count($entity->getMocks()));
                }
            }
        }

        //Assert remove method exists and is callable
        if (isset($args[7])) {
            $this->assertTrue(
                method_exists($entity, $removeMethod),
                'The specified remove method "' . $removeMethod . '" does not exist.'
            );
            $this->assertTrue(
                is_callable(array($entity, $removeMethod)),
                'The specified remove method "' . $removeMethod . '" is not callable.'
            );

            if (isset($args[8])) {
                $collectionCount = count($entity->getMocks());
                if (is_array($args[8])) {
                    $removeCount = count($args[8]);
                    $total = $collectionCount - $removeCount;

                    foreach ($args[8] as $item) {
                        $this->assertInternalType('boolean', $entity->$removeMethod($item));
                    }

                    $this->assertEquals($total, count($entity->getMocks()));
                } else {
                    $total = $collectionCount - 1;
                    $this->assertTrue($entity->$removeMethod($args[8]));
                    $this->assertEquals($total, count($entity->getMocks()));
                }
            }
        }
    }
}