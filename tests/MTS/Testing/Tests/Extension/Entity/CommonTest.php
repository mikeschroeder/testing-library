<?php

namespace MTS\Testing\Tests\Extension\Entity;

use MTS\Testing\Extension\Entity\Common as CommonExtension;
use MTS\Testing\Tests\Mock\MockEntity;

class CommonTest extends \PHPUnit_Framework_TestCase
{

    protected $entity;
    protected $extension;

    public function setUp()
    {
        $this->entity = new MockEntity();
        $this->extension = new CommonExtension();
    }

    /**
     * @expectedException        \PHPUnit_Framework_ExpectationFailedException
     * @expectedExceptionMessage Entity does not contain the specified getter method "getNonExistent".
     */
    public function testNonExistentGetter()
    {
        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getNonExistent'
        );
    }

    public function testGetterOnly()
    {
        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getId'
        );
    }

    /**
     * @expectedException        \PHPUnit_Framework_ExpectationFailedException
     * @expectedExceptionMessage Entity does not contain the specified setter method "setId".
     */
    public function testGetterAndNonExistentSetter()
    {
        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getId',
            null,
            'setId'
        );
    }

    public function testGetterAndSetterChainable()
    {
        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getChainable',
            null,
            'setChainable',
            'testing'
        );
    }

    /**
     * @expectedException        \PHPUnit_Framework_ExpectationFailedException
     * @expectedExceptionMessage Entity setter (setNonChainable) is not chainable.
     */
    public function testGetterAndSetterNonChainableAsChainable()
    {
        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getNonChainable',
            null,
            'setNonChainable',
            'testing'
        );
    }

    public function testGetterAndSetterNonChainableAsNonChainable()
    {
        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getNonChainable',
            null,
            'setNonChainable',
            'testing',
            false
        );
    }

    public function testGetterAndSetterManipulated()
    {
        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getManipulated',
            null,
            'setManipulated',
            'testing',
            true,
            'TESTING'
        );
    }

    /**
     * Failure because we can not check the default on properties set during instantiation
     *
     * @expectedException \PHPUnit_Framework_ExpectationFailedException
     */
    public function testGetterAndSetterDefaultDate()
    {
        $dateTime = new \DateTime();

        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getCreated',
            null,
            'setCreated',
            $dateTime,
            true
        );
    }

    public function testGetterAndSetterDefaultDateDefaultSet()
    {
        $dateTime = new \DateTime();

        $this->extension->setEntityPropertyDefaultValue($this->entity, 'created', $dateTime);

        $this->extension->assertEntityGetterSetter(
            $this->entity,
            'getCreated',
            $dateTime,
            'setCreated',
            new \DateTime(),
            true
        );
    }

    public function testEntityGetterSetterArray()
    {
        $this->extension->arrayAssertionRunner(
            $this->entity,
            array(
                array('getId'),
                array('getChainable', null, 'setChainable', 'value'),
                array('getNonChainable', null, 'setNonChainable', 'value', false),
                array('getManipulated', null, 'setManipulated', 'testing', true, 'TESTING')
            ),
            array($this->extension, 'assertEntityGetterSetter')
        );
    }
}