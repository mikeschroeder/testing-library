<?php

namespace MTS\Testing\Tests\Extension\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use MTS\Testing\Extension\Entity\Common as CommonExtension;
use MTS\Testing\Extension\Entity\Doctrine as DoctrineExtension;
use MTS\Testing\Tests\Mock\MockDoctrineEntity;

class DoctrineTest extends \PHPUnit_Framework_TestCase
{
    protected $entity;
    protected $doctrineExtension;
    protected $commonExtension;

    public function setUp()
    {
        $this->entity = new MockDoctrineEntity();
        $this->doctrineExtension = new DoctrineExtension();
        $this->commonExtension = new CommonExtension();
    }

    public function testEntityCollectionValidArrays()
    {
        $collection = new ArrayCollection();

        $setterValue = new ArrayCollection(array(
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
        ));

        $addRemoveValue = array(
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
        );

        $this->commonExtension->setEntityPropertyDefaultValue($this->entity, 'mocks', $collection);
        $this->doctrineExtension->assertEntityCollection($this->entity, 'getMocks', $collection, 'setMocks', $setterValue, 'addMock', $addRemoveValue, 'removeMock', $addRemoveValue);
    }

    public function testEntityCollectionValidIndividualObjects()
    {
        $collection = new ArrayCollection();

        $setterValue = new ArrayCollection(array(
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
        ));

        $addRemoveValue = new MockDoctrineEntity();

        $this->commonExtension->setEntityPropertyDefaultValue($this->entity, 'mocks', $collection);
        $this->doctrineExtension->assertEntityCollection($this->entity, 'getMocks', $collection, 'setMocks', $setterValue, 'addMock', $addRemoveValue, 'removeMock', $addRemoveValue);
    }
}