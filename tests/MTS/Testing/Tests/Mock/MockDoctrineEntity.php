<?php

namespace MTS\Testing\Tests\Mock;

use Doctrine\Common\Collections\ArrayCollection;

class MockDoctrineEntity extends MockEntity
{
    protected $mocks;

    public function __construct()
    {
        $this->mocks = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @param ArrayCollection $mocks
     * @return $this
     */
    public function setMocks(ArrayCollection $mocks)
    {
        $this->mocks = $mocks;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMocks()
    {
        return $this->mocks;
    }

    /**
     * @param MockDoctrineEntity $mock
     * @return bool
     */
    public function addMock(MockDoctrineEntity $mock)
    {
        return $this->mocks->add($mock);
    }

    /**
     * @param MockDoctrineEntity $mock
     * @return bool
     */
    public function removeMock(MockDoctrineEntity $mock)
    {
        return $this->mocks->removeElement($mock);
    }

}