<?php

namespace MTS\Testing\Tests\Traits\Entity;

use MTS\Testing\Traits\Entity;
use MTS\Testing\Tests\Mock\MockEntity;

class CommonTest extends \PHPUnit_Framework_TestCase
{
    /*
     * - NOTE -
     * Trait is loaded directly on the test case.
     */
    use Entity\Common;

    protected $entity;

    public function setUp()
    {
        $this->entity = new MockEntity();
    }

    /**
     * @expectedException        \PHPUnit_Framework_ExpectationFailedException
     * @expectedExceptionMessage Entity does not contain the specified getter method "getNonExistent".
     */
    public function testNonExistentGetter()
    {
        $this->assertEntityGetterSetter(
            $this->entity,
            'getNonExistent'
        );
    }

    public function testGetterOnly()
    {
        $this->assertEntityGetterSetter(
            $this->entity,
            'getId'
        );
    }

    /**
     * @expectedException        \PHPUnit_Framework_ExpectationFailedException
     * @expectedExceptionMessage Entity does not contain the specified setter method "setId".
     */
    public function testGetterAndNonExistentSetter()
    {
        $this->assertEntityGetterSetter(
            $this->entity,
            'getId',
            null,
            'setId'
        );
    }

    public function testGetterAndSetterChainable()
    {
        $this->assertEntityGetterSetter(
            $this->entity,
            'getChainable',
            null,
            'setChainable',
            'testing'
        );
    }

    /**
     * @expectedException        \PHPUnit_Framework_ExpectationFailedException
     * @expectedExceptionMessage Entity setter (setNonChainable) is not chainable.
     */
    public function testGetterAndSetterNonChainableAsChainable()
    {
        $this->assertEntityGetterSetter(
            $this->entity,
            'getNonChainable',
            null,
            'setNonChainable',
            'testing'
        );
    }

    public function testGetterAndSetterNonChainableAsNonChainable()
    {
        $this->assertEntityGetterSetter(
            $this->entity,
            'getNonChainable',
            null,
            'setNonChainable',
            'testing',
            false
        );
    }

    public function testGetterAndSetterManipulated()
    {
        $this->assertEntityGetterSetter(
            $this->entity,
            'getManipulated',
            null,
            'setManipulated',
            'testing',
            true,
            'TESTING'
        );
    }

    /**
     * Failure because we can not check the default on properties set during instantiation
     *
     * @expectedException \PHPUnit_Framework_ExpectationFailedException
     */
    public function testGetterAndSetterDefaultDate()
    {
        $dateTime = new \DateTime();

        $this->assertEntityGetterSetter(
            $this->entity,
            'getCreated',
            null,
            'setCreated',
            $dateTime,
            true
        );
    }

    public function testGetterAndSetterDefaultDateDefaultSet()
    {
        $dateTime = new \DateTime();

        $this->setEntityPropertyDefaultValue($this->entity, 'created', $dateTime);

        $this->assertEntityGetterSetter(
            $this->entity,
            'getCreated',
            $dateTime,
            'setCreated',
            new \DateTime(),
            true
        );
    }

    public function testEntityGetterSetterArray()
    {
        $this->arrayAssertionRunner(
            $this->entity,
            [
                ['getId'],
                ['getChainable', null, 'setChainable', 'value'],
                ['getNonChainable', null, 'setNonChainable', 'value', false],
                ['getManipulated', null, 'setManipulated', 'testing', true, 'TESTING']
            ],
            [$this, 'assertEntityGetterSetter']
        );
    }
}