<?php

namespace MTS\Testing\Tests\Traits\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use MTS\Testing\Tests\Mock\ProtectedMockDoctrineEntity;
use MTS\Testing\Traits\Entity;
use MTS\Testing\Tests\Mock\MockDoctrineEntity;

class DoctrineTest extends \PHPUnit_Framework_TestCase
{
    /*
     * - NOTE -
     * Trait is loaded directly on the test case.
     */
    use Entity\Doctrine;
    use Entity\Common;

    protected $entity;

    public function setUp()
    {
        $this->entity = new MockDoctrineEntity();
    }

    public function testEntityCollectionValidArrays()
    {
        $collection = new ArrayCollection();

        $setterValue = new ArrayCollection(array(
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
        ));

        $addRemoveValue = array(
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
        );

        $this->setEntityPropertyDefaultValue($this->entity, 'mocks', $collection);
        $this->assertEntityCollection($this->entity, 'getMocks', $collection, 'setMocks', $setterValue, 'addMock', $addRemoveValue, 'removeMock', $addRemoveValue);
    }

    public function testEntityCollectionValidIndividualObjects()
    {
        $collection = new ArrayCollection();

        $setterValue = new ArrayCollection(array(
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
            new MockDoctrineEntity(),
        ));

        $addRemoveValue = new MockDoctrineEntity();

        $this->setEntityPropertyDefaultValue($this->entity, 'mocks', $collection);
        $this->assertEntityCollection($this->entity, 'getMocks', $collection, 'setMocks', $setterValue, 'addMock', $addRemoveValue, 'removeMock', $addRemoveValue);
    }
}